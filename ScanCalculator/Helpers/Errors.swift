//
//  Errors.swift
//  ScanCalculator
//
//  Created by habagat on 4/6/22.
//

import Foundation

enum ServiceError: Error {
    case unableToPerformRequest
    case noOperator
    case missingParameter
    case unableToFindText
    case unableToFindImage
}

extension ServiceError: CustomNSError {
    var errorInfo: [String: String] {
        switch self {
        case .unableToPerformRequest:
            return [NSLocalizedDescriptionKey: "Unable to perform request"]
        case .noOperator:
            return [NSLocalizedDescriptionKey: "Unable to find available operator"]
        case .missingParameter:
            return [NSLocalizedDescriptionKey: "Missing parameter"]
        case .unableToFindText:
            return [NSLocalizedDescriptionKey: "Unable to find text on the provided image"]
        case .unableToFindImage:
            return [NSLocalizedDescriptionKey: "Unable to find image"]
        }
    }
}
