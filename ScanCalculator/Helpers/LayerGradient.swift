//
//  LayerGradient.swift
//  ScanCalculator
//
//  Created by habagat on 4/6/22.
//

import UIKit

extension UIView {
    func setViewGradient(range: [CGColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: self.bounds.origin.x,
                                     y: self.bounds.origin.y,
                                     width: self.bounds.width * 3,
                                     height: self.bounds.height)

        // gradientLayer.type = .axial
        gradientLayer.colors = range
//        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
//        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.locations = [0, 0.5, 1]
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}

extension UIButton {
    func setButtonGradient(range: [CGColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: self.bounds.origin.x,
                                     y: self.bounds.origin.y,
                                     width: self.bounds.width * 3,
                                     height: self.bounds.height)
        // gradientLayer.type = .axial
        gradientLayer.colors = range
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        gradientLayer.locations = [0, 0.5, 1]
        gradientLayer.cornerRadius = 5
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
