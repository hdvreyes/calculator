//
//  TextRecognition.swift
//  ScanCalculator
//
//  Created by habagat on 4/5/22.
//

import Foundation
import Vision
import UIKit
import Combine

class TextRecognition {
    let recognitionLevel = VNRequestTextRecognitionLevel.accurate
    let operators = ["+", "-", "x", "*", "/", "%"]

    // MARK: - Based on the requirements
    // - only very simple 2 argument operations must be supported (+,-,*,/) (i.e. 2+2, 3-1, etc.)
    // - if there are multiple expressions in the picture take the first one
    func startProcess(withImage image: UIImage?) -> Future<Parameter, ServiceError> {
        return Future { promise in
            if let image = image,
               let cgImage = image.cgImage {
                let requestHandler = VNImageRequestHandler(cgImage: cgImage,
                                                           options: [:])

                let request = VNRecognizeTextRequest { request, error in
                    guard let observations = request.results as? [VNRecognizedTextObservation],
                          error == nil else { return }

                    let textFromImage = observations.compactMap({ $0.topCandidates(1).first?.string }).joined(separator: ",")

                    if !textFromImage.isEmpty {
                        // NOTE: A simpler solution would be to split the textFromImage using delimeter(assuming there is a delimeter)
                        // and then take the first expression and and use the NSExpression to calculate
                        // however, we have to consider future adjustments. So will proceed the steps below

                        let removedSpaces = textFromImage.components(separatedBy: .whitespaces).joined()

                        // Check for the number of operators in the string and sort
                        let scannedOperators = removedSpaces.positionsOf(operators: self.operators)
                        let sortedKeys = scannedOperators.sorted(by: { $0.0 < $1.0 })

                        // Take the first operator - were only required to have one expression
                        guard let operatorPosition = sortedKeys.first?.key,
                              let operatorValue = sortedKeys.first?.value.lowercased() else {
                                  promise(.failure(ServiceError.noOperator))
                                  return
                        }

                        let firstParameter = self.getFirstParameterOfExpression(withText: removedSpaces,
                                                                                operatorPosition: operatorPosition)
                        let secondParameter = self.getSecondParameterOfExpression(withText: removedSpaces,
                                                                                  operatorPosition: operatorPosition)
                        let formatOperator = operatorValue.replacingOccurrences(of: "x", with: "*")

                        guard let firstParameter = firstParameter,
                              let secondParameter = secondParameter else {
                                  promise(.failure(ServiceError.missingParameter))
                                  return
                              }

                        promise(.success(Parameter(firstParameter: firstParameter,
                                                   secondParameter: secondParameter,
                                                   operation: formatOperator)))
                    } else {
                        promise(.failure(ServiceError.unableToFindText))
                    }
                }
                request.recognitionLevel = self.recognitionLevel

                do {
                    try requestHandler.perform([request])
                } catch {
                    promise(.failure(ServiceError.unableToPerformRequest))
                }
            } else {
                promise(.failure(ServiceError.unableToFindImage))
            }
        }
    }
}

extension TextRecognition {
    // MARK: - FIRST PARAMETER
    // Get the index to which the operator is present
    // Get the first character until the operator [offsetRight]
    // By doing this we can calculate values with multiple digits
    private func getFirstParameterOfExpression(withText text: String,
                                               operatorPosition: Int) -> Int? {
        let offset = operatorPosition - 1
        let endPosition = text.index(text.startIndex, offsetBy: offset)
        return Int(text[text.startIndex...endPosition]) ?? 0
    }

    // MARK: - SECOND PARAMETER
    // Get the index to which the operator is present this is be the start
    // Get first occurrence of delimeter (",") will be the end of substring
    // By doing this we can calculate values with multiple digits
    private func getSecondParameterOfExpression(withText text: String,
                                                operatorPosition: Int) -> Int? {
        let offset = operatorPosition + 1
        guard let delimeter = text.firstIndex(of: ",") else {
            let index = text.index(text.startIndex, offsetBy: offset)
            return Int(text[index..<text.endIndex]) ?? 0
        }
        let index = text.index(text.startIndex, offsetBy: offset)

        return Int(text[index..<delimeter]) ?? 0
    }
}
