//
//  Colors.swift
//  ScanCalculator
//
//  Created by habagat on 4/7/22.
//

import UIKit

extension UIColor {
    // Yellow
    open class var cyberYellow: UIColor { return UIColor(red: 1.00, green: 0.83, blue: 0.00, alpha: 1.00) }
    open class var bananaYellow: UIColor { return UIColor(red: 1.00, green: 0.87, blue: 0.24, alpha: 1.00) }
    open class var cornYellow: UIColor { return UIColor(red: 1.00, green: 0.92, blue: 0.38, alpha: 1.00) }
    open class var flavescentYellow: UIColor { return UIColor(red: 1.00, green: 0.95, blue: 0.57, alpha: 1.00) }

    // pastel blue
    open class var middleBlueGreen: UIColor { return UIColor(red: 0.55, green: 0.85, blue: 0.78, alpha: 1.00) }
    open class var pearlAqua: UIColor { return UIColor(red: 0.45, green: 0.80, blue: 0.77, alpha: 1.00) }
    open class var seaSerpent: UIColor { return UIColor(red: 0.35, green: 0.76, blue: 0.77, alpha: 1.00) }
    open class var moonstone: UIColor { return UIColor(red: 0.25, green: 0.71, blue: 0.76, alpha: 1.00) }
    open class var pacificBlue: UIColor { return UIColor(red: 0.15, green: 0.66, blue: 0.76, alpha: 1.00) }
    open class var blueGreen: UIColor { return UIColor(red: 0.05, green: 0.61, blue: 0.75, alpha: 1.00) }

    // pastel orange
    open class var orangeYellow: UIColor { return UIColor(red: 1.00, green: 0.93, blue: 0.71, alpha: 1.00) }
    open class var vividOrangePeel: UIColor { return UIColor(red: 1.00, green: 1.00, blue: 0.88, alpha: 1.00) }
    open class var americanOrange: UIColor { return UIColor(red: 1.00, green: 0.54, blue: 0.02, alpha: 1.00) }
    open class var orangeRed: UIColor { return UIColor(red: 1.00, green: 0.41, blue: 0.12, alpha: 1.00) }
    open class var tartOrange: UIColor { return UIColor(red: 0.96, green: 0.27, blue: 0.27, alpha: 1.00) }

    // Purple and blue
    open class var middleBluePurple: UIColor { return UIColor(red: 0.55, green: 0.45, blue: 0.75, alpha: 1.00) }
    open class var crayolaBlueViolet: UIColor { return UIColor(red: 0.47, green: 0.39, blue: 0.71, alpha: 1.00) }
    open class var plumpPurpleLight: UIColor { return UIColor(red: 0.40, green: 0.33, blue: 0.68, alpha: 1.00) }
    open class var plumpPurpleDark: UIColor { return UIColor(red: 0.32, green: 0.27, blue: 0.65, alpha: 1.00) }
    open class var pigmentBlueLight: UIColor { return UIColor(red: 0.25, green: 0.21, blue: 0.62, alpha: 1.00) }
    open class var pigmentBlueDark: UIColor { return UIColor(red: 0.17, green: 0.15, blue: 0.59, alpha: 1.00) }

    // Red
    open class var spanishRed: UIColor { return UIColor(red: 0.93, green: 0.00, blue: 0.14, alpha: 1.00) }
    open class var bloodOrange: UIColor { return UIColor(red: 0.83, green: 0.01, blue: 0.13, alpha: 1.00) }
    open class var cadmiumPurple: UIColor { return UIColor(red: 0.73, green: 0.02, blue: 0.11, alpha: 1.00) }
    open class var spartanCrimson: UIColor { return UIColor(red: 0.64, green: 0.04, blue: 0.10, alpha: 1.00) }
    open class var redDevil: UIColor { return UIColor(red: 0.54, green: 0.05, blue: 0.08, alpha: 1.00) }
    open class var uPMaroon: UIColor { return UIColor(red: 0.44, green: 0.06, blue: 0.07, alpha: 1.00) }

    // Green
    open class var oceanGreenLight: UIColor { return UIColor(red: 0.38, green: 0.75, blue: 0.56, alpha: 1.00) }
    open class var oceanGreenDark: UIColor { return UIColor(red: 0.30, green: 0.70, blue: 0.60, alpha: 1.00) }
    open class var keppel: UIColor { return UIColor(red: 0.23, green: 0.65, blue: 0.64, alpha: 1.00) }
    open class var lightSeaGreen: UIColor { return UIColor(red: 0.15, green: 0.59, blue: 0.67, alpha: 1.00) }
    open class var cyanCornflowerBlue: UIColor { return UIColor(red: 0.07, green: 0.54, blue: 0.71, alpha: 1.00) }
    open class var oceanBoatBlue: UIColor { return UIColor(red: 0.00, green: 0.49, blue: 0.75, alpha: 1.00) }
}
