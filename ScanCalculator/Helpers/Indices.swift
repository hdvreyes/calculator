//
//  Indices.swift
//  ScanCalculator
//
//  Created by habagat on 4/5/22.
//

import Foundation

extension String {
    func positionsOf(operators: [String]) -> [Int: String] {
        var hash: [Int: String] = [:]
        operators.forEach { string in
            var searchStartIndex = self.startIndex

            while searchStartIndex < self.endIndex,
                  let range = self.range(of: string, range: searchStartIndex..<self.endIndex),
                  !range.isEmpty {

                let index = distance(from: self.startIndex, to: range.lowerBound)
                hash[index] = string
                searchStartIndex = range.upperBound
            }
        }
        return hash
    }
}
