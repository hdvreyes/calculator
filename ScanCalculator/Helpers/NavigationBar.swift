//
//  NavigationBar.swift
//  ScanCalculator
//
//  Created by habagat on 4/6/22.
//

import UIKit

extension UINavigationBar {
    func customBarSetup(backgroundColor: UIColor?) {
        let background = backgroundColor ?? .crayolaBlueViolet
        self.prefersLargeTitles = true
        self.standardAppearance = setupAppearance(backgroundColor: background)
        self.scrollEdgeAppearance = setupAppearance(backgroundColor: background)
        self.addSubview(setupTitle())
    }

    func setupGradient(range: [CGColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(x: self.bounds.origin.x,
                                     y: self.bounds.origin.y,
                                     width: self.bounds.width * 3,
                                     height: self.bounds.height)

        // gradientLayer.type = .axial
        gradientLayer.colors = range
        gradientLayer.locations = [0, 0.5, 1]
        self.layer.insertSublayer(gradientLayer, at: 0)
    }

    private func setupTitle() -> UILabel {
        let titleLabel = UILabel(frame: CGRect(x: 20,
                                               y: 10,
                                               width: self.bounds.width,
                                               height: self.bounds.height))
        titleLabel.textColor = .white
        titleLabel.font = UIFont(name: "HelveticaNeue", size: 20)
        titleLabel.text = "Text Recognition"
        return titleLabel
    }

    private func setupAppearance(backgroundColor: UIColor) -> UINavigationBarAppearance {
        let navBarAppearance = UINavigationBarAppearance()
        navBarAppearance.configureWithOpaqueBackground()
        navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        navBarAppearance.shadowColor = .clear
        navBarAppearance.backgroundColor = backgroundColor
        return navBarAppearance
    }
}
