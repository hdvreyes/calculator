//
//  ResultsContainer.swift
//  ScanCalculator
//
//  Created by habagat on 4/6/22.
//

import UIKit

public class ResultsContainer: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.sharedInit()
    }

    // initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.sharedInit()
    }

    func sharedInit() {
        let thickness: CGFloat = 0.2
        let topBorder = CALayer()
        topBorder.frame = CGRect(x: 0.0,
                                 y: 0.0,
                                 width: self.frame.size.width,
                                 height: thickness)
        topBorder.backgroundColor = UIColor.lightGray.cgColor
        self.layer.addSublayer(topBorder)
    }
}
