//
//  ExpressionsContainerView.swift
//  ScanCalculator
//
//  Created by habagat on 4/6/22.
//

import UIKit

public class ExpressionsContainerView: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.sharedInit()
    }

    // initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.sharedInit()
    }

    func sharedInit() {
        let thickness: CGFloat = 0.2
        let bottomBorder = CALayer()
        bottomBorder.frame = CGRect(x: 0,
                                    y: self.frame.size.height - thickness,
                                    width: self.frame.size.width,
                                    height: thickness)
        bottomBorder.backgroundColor = UIColor.lightGray.cgColor
        self.layer.addSublayer(bottomBorder)
    }
}
