//
//  CustomButton.swift
//  ScanCalculator
//
//  Created by habagat on 4/6/22.
//

import UIKit

public class CustomButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.sharedInit()
    }

    // initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.sharedInit()
    }

    func sharedInit() {
        self.clipsToBounds = true
        self.layer.cornerRadius = 5
    }
}
