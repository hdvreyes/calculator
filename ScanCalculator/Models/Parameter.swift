//
//  Parameter.swift
//  ScanCalculator
//
//  Created by habagat on 4/6/22.
//

struct Parameter {
    let firstParameter: Int
    let secondParameter: Int
    let operation: String
}
