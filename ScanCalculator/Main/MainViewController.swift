//
//  MainViewController.swift
//  ScanCalculator
//
//  Created by habagat on 4/5/22.
//

import UIKit
import Vision
import Combine

class MainViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var navExtensionView: UIView!
    @IBOutlet weak var resultsContainerView: InformationCardView!
    @IBOutlet weak var cameraButton: CustomButton!
    @IBOutlet weak var browserButton: CustomButton!
    @IBOutlet weak var expressionLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var expressionsContainer: ExpressionsContainerView!
    @IBOutlet weak var resultsContainer: ResultsContainer!
    @IBOutlet weak var resultsOverlay: InformationCardView!
    @IBOutlet weak var galleryLabel: UILabel!
    @IBOutlet weak var galleryTipImage: UIImageView!
    @IBOutlet weak var openCameraLabel: UILabel!
    @IBOutlet weak var cameraTipImage: UIImageView!
    @IBOutlet weak var bothFeatureLabel: UILabel!
    @IBOutlet weak var bothFeatureTipImage: UIImageView!
    @IBOutlet weak var overLabel: UILabel!

    var imagePicker: UIImagePickerController!

    var textRecogitionObserver: AnyCancellable?
    var textRecognition: TextRecognition!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupImagePicker()
        setupTheme()
        setupFeature()
        setupButtons()
        textRecognition = TextRecognition()
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            textRecogitionObserver = textRecognition.startProcess(withImage: image)
                .sink(receiveCompletion: { completion in
                    switch completion {
                    case .finished:
                        // Update UI elements
                        self.expressionLabel.isHidden = false
                        self.resultLabel.isHidden = false
                        self.resultsOverlay.isHidden = true
                    case .failure(let error):
                        let serviceErr = error as ServiceError
                        self.overLabel.text = serviceErr.errorInfo.first?.value
                    }
                }, receiveValue: { [weak self] parameter in
                    guard let self = self else { return }
                    self.calculateExpression(parameter: parameter)
                })
        }
        dismiss(animated: true, completion: nil)
    }
}

extension MainViewController {

    private func setupImagePicker() {
        imagePicker = UIImagePickerController()
        imagePicker.delegate = self
    }

    @IBAction func openImageScanner(_ sender: UIButton) {
        let isCameraAccessible = UIImagePickerController.isSourceTypeAvailable(.camera)
        if isCameraAccessible {
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    @IBAction func openImageGallery(_ sender: UIButton) {
        let isGalleryAccessible = UIImagePickerController.isSourceTypeAvailable(.photoLibrary)
        if isGalleryAccessible {
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }

    private func calculateExpression(parameter: Parameter) {
        let stringExpression = "\(parameter.firstParameter)\(parameter.operation)\(parameter.secondParameter)"
        let expr = NSExpression(format: stringExpression)
        expressionLabel.text = stringExpression
        if let result = expr.expressionValue(with: nil, context: nil) as? NSNumber {
            resultLabel.text = "\(result)"
        } else {
            resultLabel.text = "0"
        }
    }

    // MARK: - Key-Value can be set on Info.plist
    private func setupTheme() {
        guard let parameter = Bundle.main.infoDictionary?["Theme"] as? String else { return }
        let theme = Theme.init(rawValue: parameter)
        let navigationExtensionColors = theme!.navigationExtensionColors.map { $0.cgColor }
        let navigationBarColors = theme!.navigationBarColors.map { $0.cgColor }
        let navigationBarColor = theme!.navigationBarColor
        navExtensionView.setViewGradient(range: navigationExtensionColors)
        self.navigationController?.navigationBar.customBarSetup(backgroundColor: navigationBarColor)
        self.navigationController?.navigationBar.setupGradient(range: navigationBarColors)
    }

    // MARK: - Key-Value can be set on Info.plist
    private func setupFeature() {
        guard let parameter = Bundle.main.infoDictionary?["Features"] as? String else { return }
        let feature = Feature.init(rawValue: parameter)

        switch feature {
        case .camera:
            cameraButton.isHidden = false
            browserButton.isHidden = true
            cameraTipImage.isHidden = false
            openCameraLabel.isHidden = false
        case .file:
            cameraButton.isHidden = true
            browserButton.isHidden = false
            galleryTipImage.isHidden = false
            galleryLabel.isHidden = false
        case .all:
            cameraButton.isHidden = false
            browserButton.isHidden = false
            bothFeatureLabel.isHidden = false
            bothFeatureTipImage.isHidden = false
        default:
            break
        }
    }

    private func setupButtons() {
        cameraButton.setButtonGradient(range: Theme.none.cameraButtoColors)
        browserButton.setButtonGradient(range: Theme.none.fileBrowseButtonColors)
    }
}

// MARK: - Conditions for theme application
// Could be placed on a different file
enum Theme: String, CaseIterable {
    case red
    case green
    case none

    var navigationExtensionColors: [UIColor] {
        switch self {
        case .red:
            return [.spartanCrimson, .redDevil, .uPMaroon]
        case .green:
            return [.lightSeaGreen, .cyanCornflowerBlue, .oceanBoatBlue]
        case .none:
            return [.plumpPurpleDark, .pigmentBlueLight, .pigmentBlueDark]
        }
    }

    var navigationBarColor: UIColor {
        switch self {
        case .red:
            return .bloodOrange
        case .green:
            return .oceanGreenDark
        case .none:
            return .crayolaBlueViolet
        }
    }

    var navigationBarColors: [UIColor] {
        switch self {
        case .red:
            return [.bloodOrange, .cadmiumPurple, .spartanCrimson]
        case .green:
            return [.oceanGreenDark, .keppel, .lightSeaGreen]
        case .none:
            return [.crayolaBlueViolet, .plumpPurpleLight, .plumpPurpleDark]
        }
    }

    var fileBrowseButtonColors: [CGColor] {
        return [UIColor.middleBlueGreen.cgColor,
                UIColor.pearlAqua.cgColor,
                UIColor.seaSerpent.cgColor,
                UIColor.moonstone.cgColor,
                UIColor.pacificBlue.cgColor,
                UIColor.blueGreen.cgColor]
    }

    var cameraButtoColors: [CGColor] {
        return [UIColor.americanOrange.cgColor,
                UIColor.orangeRed.cgColor,
                UIColor.tartOrange.cgColor]
    }
}

enum Feature: String, CaseIterable {
    case camera
    case file
    case all
}
