**Text Recognizer - Calculator**

This is a sample iOS application where it takes an image and identifies possible expressions within the image. If an expression is present, it should provide a calculation for that expression. The app uses no 3rd party library/pods, It relies on Apple Vision Library for identifying texts within the image.

This project will also demonstrate building multiple flavors of the app. Identified through themes and feature.

---

## Installing the App

1. **Clone** the master branch repository
2. Go to the project project
3. **Open** the **.xcodeproj**

---

## Building Flavors

There are 5 flavors to which the app can be build. The themes and features (requirements) can be viewed on the task file.

1. On the top most section click on the **Scheme**
2. You should see the list of **Schemes** (RedCamera, RedFileBrowser, GreenCamera, GreenFileBrowser, ScanCalculator)
3. The **Scheme** corresponds to the theme and feature.
4. Click the **Scheme** button.
5. Select a **Device**
6. Click **Run**

---

## The App

Next, you’ll the App run on either your simulator or device

1. Depending the **Scheme** you should see the theme change as well as the feature
2. **Feature File Browser** this should open your device's gallery and would let you pick the image
3. **Feature Camera** this should open your device's camera and take a picture of the image with expression
4. You should see the results on the **Main Screen** along with the expression

---

## Thank you for the opportunity
